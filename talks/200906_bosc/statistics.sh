#!/bin/bash

# A quick hack to investigate the distribution
# of packages in Debian-Med over the past few
# releases.
#
#   Steffen Moeller <moeller@debian.org> 2009
#   GPLed

set -e


# Set of internal variables caring for the counting

numoldstable=0;
numstable=0;
numtesting=0;
numunstable=0;


# Quickly inform about the state of affairs

function stats {
	cat <<EOSTATS

Here the results of the counting:

 oldstable : $numoldstable
    stable : $numstable
   testing : $numtesting
  unstable : $numunstable

EOSTATS
}

packages=$( apt-cache show med-bio | grep Recommends | cut -f 2 -d: | tr " ,|" "\n"  | sort -u | grep -v '^$' )

for i in $packages
do
	echo "Counting $i"

	dd=$( rmadison $i| grep source | cut -f3 -d\| | tr -d " " )
	for d in $dd
	do
		echo "  $d"

		if [ "stable" == "$d" -o "oldstable" == "$d"  -o "testing" == "$d" -o "unstable" == "$d" ]; then
			let "num$d += 1"
		elif [ "etch-m68k" != "$d" ]; then
			echo "Unprepared for '$d'.\n";
		fi
	done
	stats
done

